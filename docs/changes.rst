==========================
Quarrel parser changes log
==========================

This is the changes log for quarrel_parser. See the `README <../README.rst>`_.

v0.4.3 ????-??-??
-----------------

* Renames to quarrel_parser to avoid conflicts. Also updates to Nim 0.18.0.

v0.4.2 2015-06-28
-----------------

* Marked project as dead.
* Fixed compilation issues with nim 0.11.2.
* Fixed Nim 0.10.2 deprecation warnings.

v0.4.0 2015-01-06
-----------------

* Updated code to work with 0.9.6 nim release.
* Switched to master as development branch.
* Replaced static docindex.rst with generated index.
* Replace manual tests with badger_bits nake procs.
* Replaced zip library with OS command to build redistributables.
* Documented constructor syntax alternative to new_parsed_parameter template.
* Updated to Nim 0.10.2 and Nimble.
* Fixed deprecation warnings.

v0.2.0 2013-12-22
-----------------

* Adopted git-flow.
* Changed documentation to rst.
* Integrated examples as documentation.
* Added zip with HTML docs as binary download.

v0.1.1 2013-04-13
-----------------

* Fixes testing for options with various names.

v0.1.0 2013-03-10
-----------------

* Initial release.
