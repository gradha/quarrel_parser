=====================
Quarrel parser readme
=====================

The `Nim programming language <http://nim-lang.org>`_ provides the `parseopt
module <http://nim-lang.org/parseopt.html>`_ to parse options from the command
line. I found this module lacking, used to python modules like `optparse
<http://docs.python.org/2/library/optparse.html>`_ or `argparse
<http://docs.python.org/3/library/argparse.html>`_.  This module tries to
provide similar functionality to prevent you from writing command line parsing
and let you concentrate on providing the best possible experience for your
users.

Alternatives to this module are:

* `commandeer <https://github.com/fenekku/commandeer>`_.
* `docopt <https://github.com/docopt/docopt.nim>`_.

License
=======

`MIT license <LICENSE.rst>`_.


Changes
=======

This is development version 0.4.3. For a list of changes see the
`docs/changes.rst <docs/changes.rst>`_ file.
